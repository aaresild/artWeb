var urli = window.location.href;
var id = urli.split('?')[1];
console.log(id);

var sessId = sessionStorage.getItem('sessionId');
var membId = sessionStorage.getItem('membId');
console.log("session id=" + sessId + " membId=" + membId);
sessionStorage.setItem('membId', membId);

var exhibition;
function getExhibitionById() {
	$
			.ajax({
				url : "http://localhost:8080/ArtWeb/rest/exhibitions/" + id,
				type : "GET",
				contentType : "application/json; charset=utf-8",
				success : function(exhibition) {
					var tableBody = document
							.getElementById("exhibitionOnExhibitionPage");
					var tableContent = "";

					tableContent = tableContent
							+ "<tr><td>"
							+ checkIfNullOrUndefined(exhibition.title)
							+ "</td><td>"
							+ checkIfNullOrUndefined(exhibition.description)
							+ "</td><td>"
							+ checkIfNullOrUndefined(exhibition.location)
							+ "</td><td>"
							+ checkIfNullOrUndefined(exhibition.eventDate
									.substr(0, exhibition.eventDate
											.lastIndexOf("T")))
							+ "</td><td>"
							+ checkIfNullOrUndefined(exhibition.ageRestriction)
							+ "</td><td><button type='button' onClick='deleteExhibition("
							+ exhibition.idExhibition
							+ ")'>Delete</button> "
							+ "<button type='button' id='modifyButton' onClick='unhideButton(fillModifyForm("
							+ exhibition.idExhibition
							+ "))'>Modify </button> "
							+ "<button type='button' onClick='addExhibitionToMember("
							+ exhibition.idExhibition
							+ ")'>Add to list</button>" + "</td></tr>";
					tableBody.innerHTML = tableContent;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					console.log(textStatus);
				}
			});
}

getExhibitionById();

function checkIfNullOrUndefined(value) {
	if (value != null && value != undefined && value != "null") {
		return value;
	} else {
		return "";
	}
}

var allExhibitions;
function getAllExhibitions() {
	$.getJSON("http://localhost:8080/ArtWeb/rest/exhibitions/", function(
			exhibition) {
		allExhibitions = exhibition;
		console.log(exhibition);
	});
}
getAllExhibitions();

function fillModifyForm(ExhibitionId) {
	for (var i = 0; i < allExhibitions.length; i++) {
		if (allExhibitions[i].idExhibition == ExhibitionId) {
			document.getElementById("idExhibitionModify").value = ExhibitionId;
			document.getElementById("titleModify").value = allExhibitions[i].title;
			document.getElementById("descriptionModify").value = allExhibitions[i].description;
			document.getElementById("locationModify").value = allExhibitions[i].location;
			document.getElementById("eventDateModify").value = allExhibitions[i].eventDate
					.substr(0, allExhibitions[i].eventDate.lastIndexOf("T"));
			document.getElementById("ageRestrictionModify").value = allExhibitions[i].ageRestriction;
		}
	}
}
function changeExhibitions() {
	var modifyExhibitionsJson = {
		"idExhibition" : document.getElementById("idExhibitionModify").value,
		"title" : document.getElementById("titleModify").value,
		"description" : document.getElementById("descriptionModify").value,
		"location" : document.getElementById("locationModify").value,
		"eventDate" : document.getElementById("eventDateModify").value
				+ "T00:00:00+00:00",
		"ageRestriction" : document.getElementById("ageRestrictionModify").value,
	}
	console.log(modifyExhibitionsJson);
	var modifyExhibitions = JSON.stringify(modifyExhibitionsJson);

	$.ajax({
		url : "http://localhost:8080/ArtWeb/rest/exhibitions/",
		type : "PUT",
		data : modifyExhibitions,
		contentType : "application/json; charset=utf-8",
		success : function() {
			getExhibitionById();
			location.reload();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

function deleteExhibition(ExhibitionId) {
	var deleteExhibitionJson = {
		"idExhibition" : ExhibitionId
	};
	$.ajax({
		url : "http://localhost:8080/ArtWeb/rest/exhibitions/",
		type : "DELETE",
		data : JSON.stringify(deleteExhibitionJson),
		contentType : "application/json; charset=utf-8",
		success : function() {
			window.location.href = 'http://localhost:8080/ArtWeb/main.html';
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

var allMembByExhibId;
function getMembersByExhibitionId() {
	$
			.getJSON(
					"http://localhost:8080/ArtWeb/rest/members/exhibmembs/"
							+ id,
					function(member) {
						allMembByExhibId = member;
						console.log(member);
						var tableBody = document
								.getElementById("membersOnExhibitionPage");
						var tableContent = "";
						for (var i = 0; i < member.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"
									+ checkIfNullOrUndefined(member[i].firstName)
									+ "</td><td>"
									+ checkIfNullOrUndefined(member[i].lastName)
									+ "</td><td>"
									+ "<button type='button' onClick='parent.location=\"http://localhost:8080/ArtWeb/member.html?membId="
									+ member[i].idMembers
									+ "\"'>Profile</button></td><td><button type='button' onClick='deleteMemberFromExhibition("
									+ member[i].idMembers
									+ ")'>Remove</button>" + "</td></tr>";
						}
						tableBody.innerHTML = tableContent;
					});
}
getMembersByExhibitionId();

function deleteMemberFromExhibition(memberId) {
	var deleteMemberFromExhibitionJson = {
		"idMembers" : memberId
	};

	$.ajax({
		url : "http://localhost:8080/ArtWeb/rest/members/exhibitions/" + id
				+ "/members/" + memberId,
		type : "DELETE",
		data : JSON.stringify(deleteMemberFromExhibitionJson),
		contentType : "application/json; charset=utf-8",
		success : function() {
			location.reload();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

function addExhibitionToMember(exhibitionId) {
	var addExhibitionToMemberJson = {
		"idExhibition" : exhibitionId
	};
	$.ajax({
		url : "http://localhost:8080/ArtWeb/rest/exhibitions/members/" + sessId
				+ "/exhibitions/" + exhibitionId,
		type : "POST",
		data : JSON.stringify(addExhibitionToMemberJson),
		contentType : "application/json; charset=utf-8",
		success : function() {
			location.reload();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

function unhideButton(y) {
	var x; 
	if (y == 'addPeople') {
		x = document.getElementById("tableVisibility");	
	} else if (y == fillModifyForm()) {		
		x = document.getElementById("modifyExhibitionsForm");
	}
	
	if (x.style.display === "none") {
		x.style.display = "block";
	} else {
		x.style.display = "none";
	}
}

function getMembersToAddExhibition() {
	$
			.getJSON(
					"http://localhost:8080/ArtWeb/rest/members/",
					function(member) {
						allMember = member;
						console.log(member);
						var tableBody = document
								.getElementById("membersToExhibition");
						var tableContent = "";

						for (var i = 0; i < member.length; i++) {
							var isExisting = false;
							for (var j = 0; j < allMembByExhibId.length; j++) {
								if (member[i].idMembers == allMembByExhibId[j].idMembers) {
									isExisting = true;
								}
							}
							if (!isExisting) {
								tableContent = tableContent
										+ "<tr><td>"
										+ "<input value='"
										+ member[i].idMembers
										+ "' type='checkbox' id='notAttendingYet'></td><td>"
										+ checkIfNullOrUndefined(member[i].firstName)
										+ "</td><td>"
										+ checkIfNullOrUndefined(member[i].lastName)
										+ "</td><td>"
										+ "<button type='button' onClick='parent.location=\"http://localhost:8080/ArtWeb/member.html?membId="
										+ member[i].idMembers
										+ "\"'>Profile</button></td></tr>";
							}
						}
						tableContent = tableContent
								+ "<tr><button type='button' onClick='addNewMembsToExhib()'>Save</button></tr>";
						tableBody.innerHTML = tableContent;
					});
}
getMembersToAddExhibition();

function addNewMembsToExhib() {
	var checkedOrNot = document.getElementsByTagName("input");
	for (var i = 0; i < checkedOrNot.length; i++) {
		if (checkedOrNot[i].checked) {
			$.ajax({
				url : "http://localhost:8080/ArtWeb/rest/exhibitions/members/"
						+ checkedOrNot[i].value + "/exhibitions/" + id,
				type : "POST",
				contentType : "application/json; charset=utf-8",
				success : function() {
					location.reload();
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					console.log(textStatus);
				}
			});
		}
	}
}