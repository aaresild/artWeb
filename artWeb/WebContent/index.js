var sessId = sessionStorage.getItem('sessionId');
console.log("session id=" + sessId);

var allMember;
function getAllMember() {
	$.getJSON("http://localhost:8080/ArtWeb/rest/members/", function(member) {
		allMember = member;		
	});
}
getAllMember();

function addNewMembers() {
	var isExisting = false;
	for (var i = 0; i < allMember.length; i++) {
		if (allMember[i].eMail == document.getElementById("eMail").value) {
			isExisting = true;
		}
	}
	if (!isExisting) {
		if (document.getElementById("password").value == document
				.getElementById("confirm password").value) {

			var newMembersJson = {

				"firstName" : document.getElementById("firstName").value,
				"lastName" : document.getElementById("lastName").value,
				"birthday" : document.getElementById("birthday").value
						+ "T00:00:00+00:00",
				"eMail" : document.getElementById("eMail").value,
				"password" : document.getElementById("password").value,

			}
		} else {
			alert("Your password does not match.")
		}
		var addMembers = JSON.stringify(newMembersJson);		
		$
				.ajax({
					url : "http://localhost:8080/ArtWeb/rest/members/",
					type : "POST",
					data : addMembers,
					contentType : "application/json; charset=utf-8",
					success : function() {
						alert("You have successfully registered! \nNow you can log in.");
						location.reload();

					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						console.log(textStatus);
					}
				});

	} else {
		alert("You have already registered with that e-mail address.")
	}
}

function loginCheck(loginEMail, loginPassword) {
	loginEMail = document.getElementById("loginEMail").value;
	loginPassword = document.getElementById("loginPassword").value;
	var login = false;
	for (var i = 0; i < allMember.length; i++) {
		if (allMember[i].eMail == loginEMail
				&& allMember[i].password == loginPassword) {
			sessionStorage.setItem('sessionId', allMember[i].idMembers);
			window.location.href = 'http://localhost:8080/ArtWeb/main.html';
			login = true;
		}
	}

	if (login == false) {
		alert("Wrong e-mail address or password");
	}
}

function endSession() {
	sessionStorage.clear();
	window.location.href = 'http://localhost:8080/ArtWeb/';
}