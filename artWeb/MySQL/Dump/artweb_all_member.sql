CREATE DATABASE  IF NOT EXISTS `artweb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artweb`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: artweb
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `all_member`
--

DROP TABLE IF EXISTS `all_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `all_member` (
  `idMember` int(5) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `birthday` date NOT NULL,
  `e-mail` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`idMember`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `all_member`
--

LOCK TABLES `all_member` WRITE;
/*!40000 ALTER TABLE `all_member` DISABLE KEYS */;
INSERT INTO `all_member` VALUES (1,'Kaspar','Tallinn','1987-03-18','kaspartall3@gmail.com','123456'),(4,'Muki','Kutsikas','1955-01-24','adfdafa@gmail.com','654ss321'),(10,'Mati','Mänd','2012-12-12','vz@dfs.ee','1243'),(11,'Marku','Tarkusekese','1800-01-02','muki@juki.ee','111222'),(13,'Kati','Ilves','2010-01-01','oor@sss.ee','456'),(17,'Marco','Strippar','2011-01-01','aaa@aa.ee','111222'),(18,'Kuku','Pai','2001-01-01','aalo@ms.ee','5555'),(19,'Mati','Kati','2011-11-11','kjds@kjs.ee','3322'),(20,'Kuki','Turuki','1991-02-20','barbara@hhh.ee','7789'),(21,'Kaupo','Aas','1980-11-11','aa@fd.ee','66666'),(23,'John','Smith','1987-10-12','john@smith.com','john'),(24,'Margarita','Treeforest','1977-12-10','maggy@go.com','maggy'),(25,'Elisabeth','Clay','1995-02-02','clay@clay.com','clay'),(37,'May','June','1966-06-04','may@may.com','may'),(38,'May','Flower','1999-11-11','flower@may.com','flower'),(39,'Libby','Brown','1980-11-01','libby@libby.fi','libby'),(40,'Kalle','Rulle','1965-11-11','kalle@kalle.ee','kalle'),(41,'Villu','Tilillu','2000-12-06','villu@aaoo.ee','villu'),(42,'Oskar','Puskar','2000-06-22','ossu@mail.ee','ossu'),(43,'Karmo','Parm','1966-03-03','parm@hot.ee','aaa'),(44,'Vinx','Black','1947-11-14','vinx@main.com','kkk'),(45,'Lorry','Sorry','1954-11-11','lorr@yahoo.com','55'),(46,'Sander','Lucioperca','1993-12-25','sander@gmail.com','sander'),(47,'Kalle','Malle','1111-11-11','malle@kalle.ee','101');
/*!40000 ALTER TABLE `all_member` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-01 12:52:52
