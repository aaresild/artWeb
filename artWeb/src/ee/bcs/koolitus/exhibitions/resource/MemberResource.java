package ee.bcs.koolitus.exhibitions.resource;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashSet;
import java.util.Set;


import ee.bcs.koolitus.exhibitions.dao.Members;

public abstract class MemberResource {

	public static Set<Members> getAllMember() {
		LinkedHashSet<Members> allMember = new LinkedHashSet<>();
		String sqlQuery = "SELECT * FROM all_member";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				allMember.add(new Members().setIdMembers(results.getInt("idMember"))
						.setFirstName(results.getString("firstname")).setLastName(results.getString("lastname"))
						.setBirthday(results.getDate("birthday")).seteMail(results.getString("e-mail"))
						.setPassword(results.getString("password")));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting allMember set: " + e.getMessage());
		}
		return allMember;
	}

	public static Members getMemberById(int memberId) {
		Members member = null;
		String sqlQuery = "SELECT * FROM all_member WHERE idMember=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, memberId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				member = new Members().setIdMembers(results.getInt("idMember"))
						.setFirstName(results.getString("firstname")).setLastName(results.getString("lastname"))
						.setBirthday(results.getDate("birthday")).seteMail(results.getString("e-mail"))
						.setPassword(results.getString("password"));

			}
		} catch (SQLException e) {
			System.out.println("Error on getting member: " + e.getMessage());
		}
		return member;
	}

	public static Members addMembers(Members member) {
		String sqlQuery = "INSERT INTO all_member (firstname, lastname, birthday, `e-mail`, password)"
				+ "VALUES (?, ?, ?, ?, ?)"; 
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery,
				Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, member.getFirstName());
			statement.setString(2, member.getLastName());
			statement.setDate(3, javaDateToSqlDate(member.getBirthday()));
			statement.setString(4, member.geteMail());
			statement.setString(5, member.getPassword());			
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				member.setIdMembers(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println("Error on adding exhibition: " + e.getMessage());
		}
		return member;
	}

	public static void updateMembers(Members member) {
		String sqlQuery = "UPDATE all_member SET firstname=?, lastname=?, birthday=?, `e-mail`=?, password=?" 
				+ "WHERE idMember=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setString(1, member.getFirstName());
			statement.setString(2, member.getLastName());
			statement.setDate(3, javaDateToSqlDate(member.getBirthday()));
			statement.setString(4, member.geteMail());
			statement.setString(5, member.getPassword());			
			statement.setInt(6, member.getIdMembers());
			Integer code = statement.executeUpdate();
			if (code != 1) {
				throw new SQLException("Something went wrong on updating member");
			}
		} catch (SQLException e) {
			System.out.println("Error on updating member: " + e.getMessage());
		}
	}

	private static Date javaDateToSqlDate(java.util.Date date) {
		return new java.sql.Date(date.getTime());
	}

	public static void deleteMembers(Members member) {
		deleteMembersById(member.getIdMembers());
	}

	public static void deleteMembersById(int idMember) {
		String sqlQuery = "DELETE from all_member WHERE idMember=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, idMember);
			Integer code = statement.executeUpdate();
			if (code != 1) {
				throw new SQLException("Something went wrong on deleting member");
			}
		} catch (SQLException e) {
			System.out.println("Error on updating member: " + e.getMessage());
		}
	}

	public static Set<Members> getMembersByExhibitionId(int ExhibitionId) {
		LinkedHashSet<Members> members = new LinkedHashSet<>();
		String sqlQuery = "SELECT * FROM exhibitions INNER JOIN artweb.memberexhibition ON exhibitions.idExhibition=idExhib INNER JOIN artweb.all_member ON memberexhibition.idMemb=idMember WHERE idExhibition=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, ExhibitionId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				Members member = new Members().setIdMembers(results.getInt("idMember"))
						.setFirstName(results.getString("firstname")).setLastName(results.getString("lastname"))
						.setBirthday(results.getDate("birthday")).seteMail(results.getString("e-mail"))
						.setPassword(results.getString("password"));
				members.add(member);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting member set: " + e.getMessage());
		}
		return members;
	}

	public static void deleteMemberFromExhibition(int exhibitionId, int memberId) {
		String sqlQuery = "DELETE FROM artweb.memberexhibition WHERE idExhib=" + exhibitionId + " AND idMemb="
				+ memberId;
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on deleting member");
			}
		} catch (SQLException e) {
			System.out.println("Error on deleting member: " + e.getMessage());
		}
	}
}