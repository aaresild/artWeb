package ee.bcs.koolitus.exhibitions.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import ee.bcs.koolitus.exhibitions.dao.Exhibitions;
import ee.bcs.koolitus.exhibitions.dao.MemberExhibition;
import ee.bcs.koolitus.exhibitions.dao.Members;

public abstract class ExhibitionResource {
	public static Set<Exhibitions> getAllExhibitions() {
		LinkedHashSet<Exhibitions> exhibitions = new LinkedHashSet<>();
		String sqlQuery = "SELECT * FROM exhibitions";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {

				Exhibitions exhibition = new Exhibitions().setIdExhibition(results.getInt("idExhibition"))
						.setTitle(results.getString("title")).setDescription(results.getString("description"))
						.setLocation(results.getString("location")).setEventDate(results.getDate("eventDate"))
						.setAgeRestriction(results.getInt("ageRestriction"));
				exhibitions.add(exhibition);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting exhibition set: " + e.getMessage());
		}

		return exhibitions;
	}

	public static Exhibitions getExhibitionById(int idExhibition) {
		Exhibitions exhibition = null;
		String sqlQuery = "SELECT * FROM exhibitions WHERE idExhibition=" + idExhibition;
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				exhibition = new Exhibitions().setIdExhibition(results.getInt("idExhibition"))
						.setTitle(results.getString("title")).setDescription(results.getString("description"))
						.setLocation(results.getString("location")).setEventDate(results.getDate("eventDate"))
						.setAgeRestriction(results.getInt("ageRestriction"));

			}
		} catch (SQLException e) {
			System.out.println("Error on getting exhibition set: " + e.getMessage());
		}

		return exhibition;

	}

	public static void updateExhibition(Exhibitions exhibition) {
		String sqlQuery = "UPDATE exhibitions SET idExhibition=" + exhibition.getIdExhibition() + ", title='"
				+ exhibition.getTitle() + "', description='" + exhibition.getDescription() + "', location='"
				+ exhibition.getLocation() + "', eventDate='" + javaDateToSqlDate(exhibition.getEventDate())
				+ "', ageRestriction=" + +exhibition.getAgeRestriction() + " WHERE idExhibition = "
				+ exhibition.getIdExhibition();

		System.out.println(sqlQuery);
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on updating exhibition.");
			}
		} catch (SQLException e) {
			System.out.println("Error on getting exhibition. set: " + e.getMessage());
		}
	}

	private static Date javaDateToSqlDate(java.util.Date eventDate) {
		return new java.sql.Date(eventDate.getTime());
	}

	public static Exhibitions addExhibition(Exhibitions exhibition) {
		String sqlQuery = "INSERT INTO exhibitions (idExhibition, title, description, location, eventDate, ageRestriction) "
				+ "VALUES ('" + exhibition.getIdExhibition() + "', '" + exhibition.getTitle() + "', '"
				+ exhibition.getDescription() + "', '" + exhibition.getLocation() + "', '"
				+ javaDateToSqlDate(exhibition.getEventDate()) + "', '" + exhibition.getAgeRestriction() + "')";
		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				exhibition.setIdExhibition(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println("Error on adding exhibition: " + e.getMessage());
		}
		return exhibition;
	}

	public static void deleteExhibition(Exhibitions exhibition) {
		String sqlQuery = "DELETE FROM exhibitions WHERE idExhibition =" + exhibition.getIdExhibition();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on deleting exhibition");
			}
		} catch (SQLException e) {
			System.out.println("Error on deleting exhibition: " + e.getMessage());
		}
	}

	public static Set<Exhibitions> getExhibitionsByMemberId(int idMember) {
		LinkedHashSet<Exhibitions> exhibitions = new LinkedHashSet<>();
		String sqlQuery = "SELECT * FROM all_member INNER JOIN artweb.memberexhibition ON all_member.idMember=idMemb INNER JOIN artweb.exhibitions ON memberexhibition.idExhib=idExhibition WHERE idMember=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, idMember);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				Exhibitions exhibition = new Exhibitions().setIdExhibition(results.getInt("idExhibition"))
						.setTitle(results.getString("title")).setDescription(results.getString("description"))
						.setLocation(results.getString("location")).setEventDate(results.getDate("eventDate"))
						.setAgeRestriction(results.getInt("ageRestriction"));
				exhibitions.add(exhibition);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting exhibition set: " + e.getMessage());
		}
		return exhibitions;
	}

	public static void deleteExhibitionFromMember(int memberId, int exhibitionId) {
		String sqlQuery = "DELETE FROM artweb.memberexhibition WHERE idMemb=" + memberId + " AND idExhib="
				+ exhibitionId;
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on deleting exhibition");
			}
		} catch (SQLException e) {
			System.out.println("Error on deleting exhibition: " + e.getMessage());
		}
	}

	public static MemberExhibition addExhibitionToMember(MemberExhibition membExhib) {
		String sqlQuery = "INSERT INTO artweb.memberexhibition (idMemb, idExhib) VALUES (?, ?)";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, membExhib.getIdMemb());
			statement.setInt(2, membExhib.getIdExhib());			
			statement.executeUpdate();			
		} catch (SQLException e) {
			System.out.println("Error on adding exhibition to member: " + e.getMessage());
		}
		return membExhib;
	}	
	
}