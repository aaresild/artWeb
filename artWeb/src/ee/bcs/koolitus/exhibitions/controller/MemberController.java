package ee.bcs.koolitus.exhibitions.controller;

import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.exhibitions.dao.Exhibitions;
import ee.bcs.koolitus.exhibitions.dao.Members;
import ee.bcs.koolitus.exhibitions.resource.ExhibitionResource;
import ee.bcs.koolitus.exhibitions.resource.MemberResource;

@Path("/members")
public class MemberController {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Set<Members> getAllMember() {
		return MemberResource.getAllMember();
	}

	@GET
	@Path("/{idMember}")
	@Produces(MediaType.APPLICATION_JSON)
	public Members getMemberById(@PathParam("idMember") int idMember) {
		return MemberResource.getMemberById(idMember);
	}
	@GET
	@Path("/exhibmembs/{idExhibition}") 
	@Produces(MediaType.APPLICATION_JSON)
	public Set<Members> getMembersByExhibitionId(@PathParam("idExhibition") int idExhibition) {
		return MemberResource.getMembersByExhibitionId(idExhibition);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Members addMembers(Members member) {
		return MemberResource.addMembers(member);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateMembers(Members member) {
		MemberResource.updateMembers(member);
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteMembers(Members member) {
		MemberResource.deleteMembers(member);
	}
	@DELETE
	@Path("/exhibitions/{exhibitionId}/members/{memberId}") 
	@Consumes(MediaType.APPLICATION_JSON) 
	public void deleteMemberFromExhibition(@PathParam("exhibitionId") int exhibitionId, @PathParam("memberId") int memberId) {
		MemberResource.deleteMemberFromExhibition(exhibitionId, memberId);
	}
}
